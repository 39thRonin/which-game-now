<?php
namespace App\Service;

use stdClass;
use Steam\Configuration;
use Steam\Runner\GuzzleRunner;
use Steam\Runner\DecodeJsonStringRunner;
use Steam\Steam;
use Steam\Utility\GuzzleUrlBuilder;
use GuzzleHttp\Client;
use Steam\Command\PlayerService\GetOwnedGames;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class SteamManager
{
    protected $steamKey;

    public function __construct(string $steamKey) {
        $this->steamKey = $steamKey;
    }

    public function getAppsBySteamId(int $steamId): array
    {
        // Setup the steam request
        $steam = new Steam(new Configuration([
            Configuration::STEAM_KEY => $this->steamKey
        ]));
        $steam->addRunner(new GuzzleRunner(new Client(), new GuzzleUrlBuilder()));
        $steam->addRunner(new DecodeJsonStringRunner());
        $ownedGamesRequest = new GetOwnedGames($steamId);
        $ownedGamesRequest->setIncludeAppInfo(true);

        // Fetch the Steam apps that belong to the Steam ID
        $appsResponse = $steam->run($ownedGamesRequest);

        // Return the apps
        $apps = [];
        foreach ($appsResponse['response']['games'] as $responseGame) {
            $app = new stdClass();
            $app->name = $responseGame['name'];
            $app->platformAppId = (string) $responseGame['appid'];
            $app->logoId = $responseGame['img_logo_url'];
            $app->iconId = $responseGame['img_icon_url'];
            $app->totalPlayTime = $responseGame['playtime_forever'];
            $apps[] = $app;
        }
        return $apps;
    }

    public function getAppDataByAppId(int $appId)
    {
        // Get the review data for the app
        $client = new Client(['base_uri' => 'https://store.steampowered.com/']);
        $url = sprintf('appreviews/%s?json=1&filter=all&language=all&review_type=all&purchase_type=all', $appId);
        $reviewResponse = $client->request('GET', $url);

        // Build the app basic object
        $appData = new stdClass();
        $appData->totalPositiveReviews = null;
        $appData->totalReviews = null;

        // Work out the app review score
        $contents = json_decode($reviewResponse->getBody()->getContents());
        if (isset($contents->query_summary)) {
            $appData->totalPositiveReviews = $contents->query_summary->total_positive;
            $appData->totalReviews = $contents->query_summary->total_reviews;
        }
        return $appData;
    }
}