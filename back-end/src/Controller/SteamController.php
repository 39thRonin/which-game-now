<?php
namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Exception;
use GuzzleHttp\Client;
use Symfony\Component\DependencyInjection\ContainerInterface;
use App\Service\SteamManager;

class SteamController
{
    protected $steamManager;

    protected $container;

    public function __construct(SteamManager $steamManager, ContainerInterface $container)
    {
        $this->steamManager = $steamManager;
        $this->container = $container;
    }

    /**
     * The apps action - fetches all apps by a steam ID
     * @Route("/steam/apps/{steamId}", name="steam_apps", requirements={"steamId"="\d+"})
     * @Method("GET")
     * @param int $steamId
     * @return JsonResponse
     */
    public function apps(int $steamId): JsonResponse
    {
        // Get the apps information
        try {
            $apps = $this->steamManager->getAppsBySteamId($steamId);
        } catch (Exception $e) {
            return new JsonResponse($e->getMessage(), Response::HTTP_NOT_FOUND, $this->getResponseHeaders());
        }

        // Build the response
        $response = new JsonResponse($apps, Response::HTTP_OK, $this->getResponseHeaders());

        // cache for 3600 seconds
        $response->setMaxAge($this->container->getParameter('app')['steam']['appsCacheAge']);

        // (optional) set a custom Cache-Control directive
        $response->headers->addCacheControlDirective('must-revalidate', true);

        return $response;
    }

    /**
     * The app action - fetches data about a single app
     * @Route("/steam/app/{appId}", name="steam_app", requirements={"appId"="\d+"})
     * @Method("GET")
     * @param $appId
     * @return JsonResponse
     */
    public function appData(int $appId): JsonResponse
    {
        // Get the app information
        try {
            $appData = $this->steamManager->getAppDataByAppId($appId);
        } catch (Exception $e) {
            return new JsonResponse($e->getMessage(), Response::HTTP_NOT_FOUND, $this->getResponseHeaders());
        }

        // Render the view
        $response = new JsonResponse($appData, Response::HTTP_OK, $this->getResponseHeaders());

        // cache for 3600 seconds
        $response->setSharedMaxAge($this->container->getParameter('app')['steam']['appCacheAge']);

        // (optional) set a custom Cache-Control directive
        $response->headers->addCacheControlDirective('must-revalidate', true);

        return $response;
    }

    protected function getResponseHeaders() {
        $environment = $this->container->get('kernel')->getEnvironment();
        if ($environment === 'dev') {
            return ['Access-Control-Allow-Origin' => '*'];
        }
        return ['Access-Control-Allow-Origin' => 'https://whichgamenow.com'];
    }
}