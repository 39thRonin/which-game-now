// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  frontendBaseUrl: 'http://localhost:4200',
  steamImagesUrl: 'https://msp.whichgamenow.com',
  backendUrl: 'http://localhost:8000',
  adSense: {
    adClient: 'ca-pub-8215419777537518',
    adSlot: 7259870550,
    adtest: 'on',
  }
};
