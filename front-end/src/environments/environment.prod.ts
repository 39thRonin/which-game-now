export const environment = {
  production: true,
  frontendBaseUrl: 'https://whichgamenow.com',
  steamImagesUrl: 'https://msp.whichgamenow.com',
  backendUrl: 'https://bff.whichgamenow.com',
  adSense: {
    adClient: 'ca-pub-8215419777537518',
    adSlot: 7259870550,
    adtest: 'off',
  }
};
