import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { SteamService } from '../../services';

/**
 * The public component
 */
@Component({
  selector: 'app-public',
  templateUrl: './public.component.html',
})
export class PublicComponent implements OnInit {

  /**
   * Creates an instance of PublicComponent.
   */
  public constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private steamService: SteamService,
  ) {}

  /**
   * Actions to perform OnInit
   */
  public ngOnInit(): void {
    this.checkForNewSteamId();
    this.checkForExistingSteamId();
  }

  /**
   * The Steam id, if we have one set
   */
  public get steamId(): string {
    return this.steamService.steamId;
  }

  /**
   * Checks to see if we have a game library id set in the address
   */
  private checkForNewSteamId() {
    this.activatedRoute.queryParams.subscribe((params: Params) => {
      const identity = params['openid.identity'];
      if (identity) {
        this.steamService.steamId = identity.substr(identity.lastIndexOf('/') + 1);
      }
    });
  }

  /**
   * Checks to see if we have a game library id set in a service
   */
  private checkForExistingSteamId() {
    if (this.steamService.steamId) {
      this.router.navigate(['/games']);
    } else {
      this.router.navigate(['']);
    }
  }
}
