import { Component } from '@angular/core';
import { environment } from '../../../environments/environment';

/**
 * The login component
 */
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
})
export class LoginComponent {

  /**
   * Provide a generated Steam login url
   */
  public get steamLoginUrl(): string {
    return `https://steamcommunity.com/openid/login`
      + `?openid.ns=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0`
      + `&openid.mode=checkid_setup`
      + `&openid.return_to=${encodeURIComponent(environment.frontendBaseUrl)}`
      + `&openid.realm=${encodeURIComponent(environment.frontendBaseUrl)}`
      + `&openid.identity=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select`
      + `&openid.claimed_id=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select`;
  }
}
