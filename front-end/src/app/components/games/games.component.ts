import { Component, OnDestroy, ViewChild, AfterViewInit } from '@angular/core';
import { SteamService } from '../../services';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { Subscription } from 'rxjs/Subscription';
import { SteamAppEntity, AppEntity } from '../../classes';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Observable } from 'rxjs/Observable';
import { finalize, map } from 'rxjs/operators';
import { forkJoin } from 'rxjs';
import { Router } from '@angular/router';

/**
 * The games component
 */
@Component({
  selector: 'app-games',
  templateUrl: './games.component.html',
})
export class GamesComponent implements AfterViewInit, OnDestroy {

  /**
   * The collection of apps we have to display
   */
  public apps: AppEntity[] = [];

  /**
   * Boolean to record if the apps are loading
   */
  public appsLoading = true;

  /**
   * Array of subscriptions we can unsubscribe from later
   */
  public subscriptions: Subscription[] = [];

  /**
   * The app filters and their default values
   */
  public filters = {
    minimumReviews: 100,
    minimumScore: 90,
    maximumPlaytime: 3600, // 1 hour
  };

  /**
   * Record which app is has been hovered over
   */
  public hoveredApp: AppEntity;

  /**
   * The view mode, can be 'cards' or 'table' currently
   * @todo make this an enum
   */
  public viewMode = 'cards';

  /**
   * The loading progress modal
   */
  @ViewChild('progressModal') private progressModal: ModalDirective;

  /**
   * Creates an instance of GamesComponent.
   */
  public constructor(
    private steamService: SteamService,
    private domSanitizer: DomSanitizer,
    private router: Router,
  ) {}

  /**
   * Actions to perform AfterViewInit
   */
  public ngAfterViewInit(): void {
    this.loadSteamApps();
  }

  /**
   * Actions to perform OnDestroy
   */
  public ngOnDestroy(): void {
    this.unsubscribe();
  }

  /**
   * App count getter
   */
  public get appCount(): number {
    return this.apps.length;
  }

  /**
   * Getter that determines the number of apps we have
   */
  public get hasApps(): boolean {
    return this.appCount > 0;
  }

  /**
   * Determine how many apps we have data loaded for
   */
  public get loadedApps(): AppEntity[] {
    return this.apps.filter((app) => app.hasLoaded);
  }

  public get appsNotLoaded(): AppEntity[] {
    return this.apps.filter((app) => !app.hasLoaded);
  }

  public get hasAppsNotLoaded(): boolean {
    return this.apps.filter((app) => !app.hasLoaded).length > 0;
  }

  /**
   * Get the link for a steam app
   */
  public getSteamAppLink(app: SteamAppEntity): SafeUrl {
    return this.domSanitizer.bypassSecurityTrustUrl(app.appUrl);
  }

  /**
   * Select which app is hovered over
   */
  public selectHoveredApp(app: AppEntity = null) {
    this.hoveredApp = app;
  }

  /**
   * Determine if an app is being hovered over
   */
  public isAppHovered(app: AppEntity): boolean {
    return this.hoveredApp === app;
  }

  /**
   * Determine if we have any apps that meet the current criteria
   */
  public get hasAppsMeetingCriteria(): boolean {
    return this.appsMeetingCriteria.length > 0;
  }

  /**
   * Get all the apps that meet the current criteria
   */
  public get appsMeetingCriteria(): AppEntity[] {

    /*if (!this.hasApps) {
      return [];
    }*/

    return this.apps.filter((app) => {
      return app.hasMinimumReviews(this.filters.minimumReviews)
        && app.hasMinimumScore(this.filters.minimumScore)
        && app.hasMaximumPlaytime(this.filters.maximumPlaytime);
    });
  }

  /**
   * Switch the view mode
   */
  public switchViewMode() {
    this.viewMode = this.viewMode === 'cards' ? 'table' : 'cards';
  }

  /**
   * React to click events on the Steam sign out button
   */
  public onClickSignOutOfSteam() {
    this.steamService.resetSteamId();
    this.router.navigate(['']);
  }

  /**
   * Load the steam apps
   */
  private loadSteamApps() {

    if (!this.steamService.steamId) {
      return;
    }

    this.appsLoading = true;
    this.progressModal.show();
    this.subscriptions['steamAppsSubscription'] = this.steamService
      .getApps(this.steamService.steamId)
      .pipe(finalize(() => {
        this.appsLoading = false;
      }))
      .subscribe(
        (data) => {
          this.apps = data;
          this.loadAppsData();
        },
        (error) => {
          // console.log(error);
        }
      );
  }

  /**
   * Load the steam apps data
   */
  private loadAppsData() {
    this.subscriptions['appsDataSubscription'] = this
      .loadAppsDataBatch()
      .pipe(finalize(() => {
        if (this.hasAppsNotLoaded) {
          this.loadAppsData();
        } else {
          this.progressModal.hide();
        }
      }))
      .subscribe(
        () => {
          this.sortApps();
        },
        (error) => {
          // console.log(error);
        }
      );
  }

  private sortApps() {
    this.apps.sort((a, b) => b.reviewsScore - a.reviewsScore );
  }

  /**
   * Load the data for a batch of apps
   */
  private loadAppsDataBatch(): Observable<any> {
    const operations: Array<Observable<any>> = [];
    for (const app of this.apps.filter((appToFilter) => !appToFilter.hasLoaded).slice(0, 10)) {
      if (app instanceof SteamAppEntity) {
        operations.push(
          this.steamService
            .getReviewDataByApp(app)
            .pipe(map((data) => {
              app.reviewData = data;
              return data;
            }))
        );
      }
    }
    return forkJoin([...operations]);
  }

  /**
   * Unsubscribe from all subscriptions
   */
  private unsubscribe() {
    this.subscriptions.forEach((sub) => {
      sub.unsubscribe();
    });
  }
}
