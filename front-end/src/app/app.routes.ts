import { Routes, RouterModule } from '@angular/router';

// Public Layouts and Components
import { LoginComponent } from './components/login';
import { GamesComponent } from './components/games';
import { PublicComponent } from './components/public';

// Public Routes
export const PUBLIC_ROUTES: Routes = [
  {
    path: '',
    component: LoginComponent,
  },
  {
    path: 'games',
    component: GamesComponent,
  },
  { path: '**', component: LoginComponent }
];

// App Routes, uses public & secure routes above
const APP_ROUTES: Routes = [
  {
    path: '',
    component: PublicComponent,
    children: PUBLIC_ROUTES
  }
];

export const routing = RouterModule.forRoot(APP_ROUTES);
