/**
 * The steam app review data interface
 */
export interface SteamAppReviewData {
  totalReviews: number;
  totalPositiveReviews: number;
}
