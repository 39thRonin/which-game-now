import { App, SteamAppReviewData } from './';

/**
 * The steam app interface
 */
export interface SteamApp extends App {
  logoId: string;
  iconId: string;
  totalPlayTime: number;
  reviewData: SteamAppReviewData;
}
