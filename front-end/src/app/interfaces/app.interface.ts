import { Platform } from '../enums';

/**
 * The app interface
 */
export interface App {
  name: string;
  platform: Platform;
  platformAppId: string;
}
