import { App } from '../interfaces';
import { Entity } from './entity.class';
import { Platform } from '../enums';

/**
 * The app entity
 */
export abstract class AppEntity extends Entity implements App {
  public name: string;
  public platform: Platform;
  public platformAppId: string;

  public abstract get hasLoaded(): boolean;

  public abstract get totalReviews(): number;
  public abstract get reviewsScore(): number;
  public abstract get totalPlaytime(): number;

  public abstract hasMinimumReviews(minimumReviews: number): boolean;
  public abstract hasMinimumScore(minimumScore: number): boolean;
  public abstract hasMaximumPlaytime(maximumPlaytime: number): boolean; // In seconds

  public get formattedTotalReviews(): string {
    return this.totalReviews / 1000 < 1 ? this.totalReviews.toFixed(0) : Math.floor(this.totalReviews / 1000) + 'k';
  }
  public get formattedScore(): string {
    return this.reviewsScore.toFixed(0);
  }
  public get formattedPlaytime(): string {
    let minutes: string = ((Math.floor(this.totalPlaytime / 60)) % 60).toString();
    if (minutes.length === 1) {
      minutes = '0' + minutes;
    }
    const hours = Math.floor(this.totalPlaytime / 60 / 60);
    return `${hours}:${minutes}`;
  }
}
