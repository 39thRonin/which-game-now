/**
 * The entity
 */
export abstract class Entity {

  /**
   * Populate the entity using a basic object
   */
  public populateFromObject(entity) {
    for (const i in entity) {
      if (entity.hasOwnProperty(i)) {
        if (typeof this[i] !== 'undefined' && this[i] !== null && typeof this[i].populateFromObject === 'function' && entity[i] !== null) {
          this[i].populateFromObject(entity[i]);
        } else {
          this[i] = entity[i];
        }
      }
    }
    return this;
  }
}
