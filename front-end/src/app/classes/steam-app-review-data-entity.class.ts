import { SteamAppReviewData } from '../interfaces';
import { Entity } from './entity.class';

export class SteamAppReviewDataEntity extends Entity implements SteamAppReviewData {
  public totalReviews: number;
  public totalPositiveReviews: number;

  public get score(): number {
    if (typeof this.totalReviews === 'undefined' || this.totalReviews === null) {
      return 0;
    }
    if (this.totalReviews === 0) {
      return 0;
    }
    return ((this.totalPositiveReviews / this.totalReviews) * 100);
  }
}
