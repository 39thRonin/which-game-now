import { SteamApp } from '../interfaces';
import { AppEntity } from './app-entity.class';
import { SteamAppReviewDataEntity } from './steam-app-review-data-entity.class';
import { Platform } from '../enums';
import { environment } from '../../environments/environment';

/**
 * The steam app entity
 */
export class SteamAppEntity extends AppEntity implements SteamApp {
  public platform: Platform = Platform.Steam;
  public logoId: string;
  public iconId: string;
  public totalPlayTime: number;
  public reviewData: SteamAppReviewDataEntity = new SteamAppReviewDataEntity();

  public get hasLoaded(): boolean {
    return typeof this.reviewData.totalReviews !== 'undefined';
  }

  public get totalReviews(): number {
    return this.reviewData.totalReviews;
  }
  public get reviewsScore(): number {
    return this.reviewData.score;
  }
  public get totalPlaytime(): number {
    return this.totalPlayTime * 60;
  }

  public hasMinimumReviews(minimumReviews: number): boolean {
    return this.reviewData.totalReviews >= minimumReviews;
  }
  public hasMinimumScore(minimumScore: number): boolean {
    return this.reviewData.score >= minimumScore;
  }
  public hasMaximumPlaytime(maximumPlaytime: number): boolean {
    return this.totalPlaytime <= maximumPlaytime;
  }

  /**
   * Alias URL for https://media.steampowered.com/steamcommunity/public/images/apps/${this.platformAppId}/${this.logoId}.jpg
   */
  public get logoUrl(): string {
    return `${environment.steamImagesUrl}/${this.platformAppId}/${this.logoId}`;
  }
  /**
   * Alias URL for https://media.steampowered.com/steamcommunity/public/images/apps/${this.platformAppId}/${this.iconId}.jpg
   */
  public get iconUrl(): string {
    return `${environment.steamImagesUrl}/${this.platformAppId}/${this.iconId}`;
  }
  public get storeUrl(): string {
    return `https://store.steampowered.com/app/${this.platformAppId}`;
  }
  public get appUrl(): string {
    return `steam://run/${this.platformAppId}`;
  }
}
