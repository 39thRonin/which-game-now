import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';
import { ModalModule } from 'ngx-bootstrap/modal';
import { LoginComponent } from '../components/login';
import { GamesComponent } from '../components/games';
import { PublicComponent } from '../components/public';

@NgModule({
  imports: [
    CommonModule, RouterModule, FormsModule, ReactiveFormsModule, ModalModule, ProgressbarModule,
  ],
  declarations: [
    PublicComponent, LoginComponent, GamesComponent
  ],
  providers: [
  ],
})
export class PublicModule { }
