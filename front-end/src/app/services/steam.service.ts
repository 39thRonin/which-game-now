import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SteamApp, SteamAppReviewData } from '../interfaces';
import { SteamAppEntity, SteamAppReviewDataEntity } from '../classes';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable()
export class SteamService {

  public constructor(private http: HttpClient) {}

  public get steamId(): string {
    return localStorage.getItem('steam-id');
  }

  public set steamId(steamId: string) {
    localStorage.setItem('steam-id', steamId);
  }

  public resetSteamId() {
    localStorage.removeItem('steam-id');
  }

  public getApps(steamId: string): Observable<SteamAppEntity[]> {
    return this.http
      .get<Array<SteamApp>>(
        `${environment.backendUrl}/steam/apps/${steamId}`
      ).pipe(
        map((response) => {
          const entities: SteamAppEntity[] = [];
          response.forEach((app) => {
            entities.push(new SteamAppEntity().populateFromObject(app));
          });
          return entities;
        })
      );
  }

  public getReviewDataByApp(app: SteamAppEntity): Observable<SteamAppReviewDataEntity> {
    const cached: SteamAppReviewData = this.getCachedAppReviewData(app);
    if (cached) {
      return of(new SteamAppReviewDataEntity().populateFromObject(cached));
    }

    return this.http
      .get<SteamAppReviewData>(
        `${environment.backendUrl}/steam/app/${app.platformAppId}`
      ).pipe(
        map((response) => {
          this.cacheAppReviewData(app, response);
          const entity = new SteamAppReviewDataEntity().populateFromObject(response);
          return entity;
        })
      );
  }

  private getCachedAppReviewData(app: SteamAppEntity) {
    return JSON.parse(localStorage.getItem(`steam-app-data-${app.platformAppId}`));
  }

  private cacheAppReviewData(app: SteamAppEntity, data: SteamAppReviewData) {
    localStorage.setItem(`steam-app-data-${app.platformAppId}`, JSON.stringify(data));
  }
}
